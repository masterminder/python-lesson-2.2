import json

def parse_json(file_name):
  with open((file_name), 'r') as f:
        text = json.load(f)
  return text

def print_recipes():
      cook_book = parse_json('recipes.json')
      for key, value in cook_book.items():
                  print_str = 'Название блюда: ' + key + '\n'

                  for num, item in enumerate(value):
                        print_str += 'Ингридиент {}: {}, Количество: {}, Мера: {}\n'.format(
                                          num,
                                          item['ingridient_name'],
                                          item['quantity'],
                                          item['measure'])
                  print(print_str)

print_recipes()